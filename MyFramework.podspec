
Pod::Spec.new do |s|
    s.name         = "MyFramework"
    s.version      = "0.0.1"
    s.summary      = "MyFramework"
    s.description  = "MyFramework"
    s.homepage     = "https://"
    s.license      = {
    :type => 'Copyright',
    :text => <<-LICENSE
        LICENSE
    }
    s.authors      = { 'Foo' => 'foo@foo.com' }
    s.source       = { :http => "https://gitlab.com/api/v4/projects/30179612/packages/generic/SDK/0.0.2/MyFramework.xcframework.zip" }
    s.platform     = :ios, '10.0'
    s.swift_version = "5.0"
    s.requires_arc = true
    s.vendored_frameworks = 'MyFramework.xcframework'
end

